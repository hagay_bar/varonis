variable "location" {
default = "westeurope"
}
 
variable "admin_username" {
default = "varonis"
}
 
variable "admin_password" {
default = "!QAZ2wsx"
}
 
variable "computer_name_Windows1" {
default = "dc"
}

variable "computer_name_Windows2" {
default = "server"
}
 
variable "rg_network" {
default = "Varonis-Network"
}
 
variable "vmsize" {
description = "VM Size"
type = "map"
default = {
    small = "Standard_DS1_v2"
    medium = "Standard_D2s_v3"
    large = "Standard_D4s_v3"
    extralarge = "Standard_D8s_v3"
    }
}
 
variable "os_ms" {
description = "os_ms"
type = "map"
default = {
    publisher = "MicrosoftWindowsServer"
    offer = "WindowsServer"
    sku = "2016-Datacenter"
    version = "latest"
    }
}