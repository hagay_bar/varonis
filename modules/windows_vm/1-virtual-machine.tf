resource "azurerm_public_ip" "pip1" {
  name                                = "myPublicIP1"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  public_ip_address_allocation        = "dynamic"
  domain_name_label                   = "${var.computer_name_Windows1}-${lower(substr("${join("", split(":", timestamp()))}", 8, -1))}"
}

resource "azurerm_network_security_group" "myterraformnsg1" {
  name                                = "myNetworkSecurityGroup1"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
    
  security_rule {
    name                              = "RDP"
    priority                          = 1001
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "3389"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }

  security_rule {
    name                       = "Allow_WinRM"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "windows_nic1" {

  name                                = "${var.computer_name_Windows1}-nic"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  network_security_group_id           = "${azurerm_network_security_group.myterraformnsg1.id}"

    ip_configuration {
      name                            = "${var.computer_name_Windows1}-ipconfig"
      subnet_id                       = "${var.subnet_id}"
      private_ip_address_allocation   = "dynamic"
      public_ip_address_id            = "${azurerm_public_ip.pip1.id}"
    }
}

resource "azurerm_virtual_machine" "windows_vm1" {

  name                                = "${var.computer_name_Windows1}"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  network_interface_ids               = ["${azurerm_network_interface.windows_nic1.id}"]
  vm_size                             = "${var.vmsize["large"]}"
  delete_os_disk_on_termination       = true
  delete_data_disks_on_termination    = true

    storage_image_reference {
      publisher                       = "${var.os_ms["publisher"]}"
      offer                           = "${var.os_ms["offer"]}"
      sku                             = "${var.os_ms["sku"]}"
      version                         = "${var.os_ms["version"]}"
    }

  storage_os_disk {

    name                              = "${var.computer_name_Windows1}-OS"
    caching                           = "ReadWrite"
    create_option                     = "FromImage"
    managed_disk_type                 = "Standard_LRS"
  }

  os_profile {

    computer_name                     = "${var.computer_name_Windows1}"
    admin_username                    = "${var.admin_username}"
    admin_password                    = "${var.admin_password}"
    custom_data                       = "${file("./files/winrm.ps1")}"
  }

  os_profile_windows_config {
    provision_vm_agent                = true

    winrm {
      protocol                        = "http"
    }

    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass                            = "oobeSystem"
      component                       = "Microsoft-Windows-Shell-Setup"
      setting_name                    = "AutoLogon"
      content                         = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    }

    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass                            = "oobeSystem"
      component                       = "Microsoft-Windows-Shell-Setup"
      setting_name                    = "FirstLogonCommands"
      content                         = "${file("./files/FirstLogonCommands.xml")}"
    }
  }

  connection {
    host                              = "${azurerm_public_ip.pip1.fqdn}"
    type                              = "winrm"
    port                              = 5985
    https                             = false
    timeout                           = "2m"
    user                              = "${var.admin_username}"
    password                          = "${var.admin_password}"
  }
}

resource "null_resource" "ProvisionStage1" {
  provisioner "local-exec" {
    command = <<EOT
    Invoke-AzVMRunCommand `
    -ResourceGroupName "${var.rg_network}" `
    -VMName "${var.computer_name_Windows1}" `
    -CommandId RunPowerShellScript `
    -ScriptPath files/config1.ps1
    EOT
    interpreter = ["PowerShell", "-Command"]
  }
  triggers = {
      "after" = "${azurerm_virtual_machine.windows_vm1.name}"
  }
}

resource "null_resource" "ProvisionStage2" {
  provisioner "local-exec" {
    command = <<EOT
    Invoke-AzVMRunCommand `
    -ResourceGroupName "${var.rg_network}" `
    -VMName "${var.computer_name_Windows1}" `
    -CommandId RunPowerShellScript `
    -ScriptPath files/config2.ps1
    EOT
    interpreter = ["PowerShell", "-Command"]
  }
  triggers = {
      "after" = "${null_resource.ProvisionStage1.id}"
  }
}

output "computer_name_Windows1" {
  value                                = "${azurerm_virtual_machine.windows_vm1.name}"
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

resource "azurerm_public_ip" "pip2" {
  name                                = "myPublicIP2"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  public_ip_address_allocation        = "dynamic"
  domain_name_label                   = "${var.computer_name_Windows2}-${lower(substr("${join("", split(":", timestamp()))}", 8, -1))}"
}

resource "azurerm_network_security_group" "myterraformnsg2" {
  name                                = "myNetworkSecurityGroup2"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
    
  security_rule {
    name                              = "RDP"
    priority                          = 1001
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "3389"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }

  security_rule {
    name                       = "Allow_WinRM"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "windows_nic2" {

  name                                = "${var.computer_name_Windows2}-nic"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  network_security_group_id           = "${azurerm_network_security_group.myterraformnsg2.id}"

    ip_configuration {
      name                            = "${var.computer_name_Windows2}-ipconfig"
      subnet_id                       = "${var.subnet_id}"
      private_ip_address_allocation   = "dynamic"
      public_ip_address_id            = "${azurerm_public_ip.pip2.id}"
    }
}

resource "azurerm_virtual_machine" "windows_vm2" {

  name                                = "${var.computer_name_Windows2}"
  location                            = "${var.location}"
  resource_group_name                 = "${var.rg_network}"
  network_interface_ids               = ["${azurerm_network_interface.windows_nic2.id}"]
  vm_size                             = "${var.vmsize["large"]}"
  delete_os_disk_on_termination       = true
  delete_data_disks_on_termination    = true
  depends_on                          = [null_resource.ProvisionStage1]

    storage_image_reference {
      publisher                       = "${var.os_ms["publisher"]}"
      offer                           = "${var.os_ms["offer"]}"
      sku                             = "${var.os_ms["sku"]}"
      version                         = "${var.os_ms["version"]}"
    }

  storage_os_disk {

    name                              = "${var.computer_name_Windows2}-OS"
    caching                           = "ReadWrite"
    create_option                     = "FromImage"
    managed_disk_type                 = "Standard_LRS"
  }

  os_profile {

    computer_name                     = "${var.computer_name_Windows2}"
    admin_username                    = "${var.admin_username}"
    admin_password                    = "${var.admin_password}"
    custom_data                       = "${file("./files/winrm.ps1")}"
  }

  os_profile_windows_config {
    provision_vm_agent                = true

    winrm {
      protocol                        = "http"
    }

    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass                            = "oobeSystem"
      component                       = "Microsoft-Windows-Shell-Setup"
      setting_name                    = "AutoLogon"
      content                         = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    }

    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass                            = "oobeSystem"
      component                       = "Microsoft-Windows-Shell-Setup"
      setting_name                    = "FirstLogonCommands"
      content                         = "${file("./files/FirstLogonCommands.xml")}"
    }
  }

  connection {
    host                              = "${azurerm_public_ip.pip2.fqdn}"
    type                              = "winrm"
    port                              = 5985
    https                             = false
    timeout                           = "2m"
    user                              = "${var.admin_username}"
    password                          = "${var.admin_password}"
  }
}

resource "null_resource" "ProvisionStage3" {
  provisioner "local-exec" {
    command = <<EOT
    Invoke-AzVMRunCommand `
    -ResourceGroupName "${var.rg_network}" `
    -VMName "${var.computer_name_Windows2}" `
    -CommandId RunPowerShellScript `
    -ScriptPath files/config3.ps1
    EOT
    interpreter = ["PowerShell", "-Command"]
  }
  triggers = {
      "after" = "${azurerm_virtual_machine.windows_vm2.name}"
      "after" = "${null_resource.ProvisionStage2.id}"
  }
}

resource "null_resource" "ProvisionStage4" {
  provisioner "local-exec" {
    command = <<EOT
    Invoke-AzVMRunCommand `
    -ResourceGroupName "${var.rg_network}" `
    -VMName "${var.computer_name_Windows2}" `
    -CommandId RunPowerShellScript `
    -ScriptPath files/config4.ps1
    EOT
    interpreter = ["PowerShell", "-Command"]
  }
  triggers = {
      "after" = "${null_resource.ProvisionStage3.id}"
  }
}

output "computer_name_Windows2" {
  value                                = "${azurerm_virtual_machine.windows_vm2.name}"
}
