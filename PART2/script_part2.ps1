$error.clear()
$test = Test-Path "c:\logs"
if ($test) {
    Remove-Item -LiteralPath "c:\logs" -Force -Recurse -ErrorAction Ignore
}    

$report = "ad_user_add_report.txt"
$date = (Get-Date).tostring("dd-MM-yyyy-hh-mm-ss")            
$foldername = New-Item -itemType Directory -Path c:\logs -Name $date 
$filename = New-Item -itemType File -Path $foldername -Name $report
$logpath = "$foldername\$report"
    
Function logwrite {
    Param ([string]$logstring)
  
    Add-content $filename -value $logstring
}

try
{

    $x = Get-ADGroup -Filter "*" | select-object *

    if ($x.Name -notcontains "VaronisAssignmen2") {

        $Params2 = @{
            Name           = 'VaronisAssignmen2'
            SamAccountName = 'VaronisAssignmen2'
            GroupCategory  = 'Security'
            GroupScope     = 'Global'
            DisplayName    = 'VaronisAssignmen2'
            Path           = 'CN=Users,DC=varonis,DC=Com'
            Description    = 'VaronisAssignmen2 members'
        }
                
        New-ADGroup @Params2
    }
    else {
        { continue }
    }

    $counter = 0
    $allusers = Get-ADUser -Filter * 

    foreach ($_ in 1..10) {
        
        $counter++
        $template = "testuser"
        $user = "$template$counter"
        if ($allusers.name -notcontains $user) {
            New-ADUser -Name $user -AccountPassword (ConvertTo-SecureString -AsPlainText "!QAZ2wsx" -Force) -passThru | Enable-ADAccount
            Add-ADGroupMember -Identity "VaronisAssignmen2" -Members $user
            $log_output = [pscustomobject]@{
                USER       = $user
                TIME_ADDED = $date
                STATE      = "Sucsses"
            }
            logwrite $log_output
        }
        else {
            $log_output = [pscustomobject]@{
                USER       = $user
                TIME_ADDED = $date
                STATE      = "user alread in AD"
            }
            logwrite $log_output
        }
    }
}

catch
{
    write-host "Caught an exception:" -ForegroundColor Red >> c:\log.txt
    write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red > c:\log.txt
    write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red > c:\log.txt
}

############################## AZ COPY ##################################


# need to login prior running script Connect-AzAccount

Install-PackageProvider -name nuget -minimumversion 2.8.5.201 -force
$az = Get-Module -ListAvailable -name "Az.Accounts"
if (!$az.Name) {
    Install-Module -Name Az -AllowClobber -Scope AllUsers -force
}

$subscription = Get-AzSubscription 
Set-AzContext -Subscription $subscription.SubscriptionId | Out-Null

#Download AzCopy
$azcopy_test = Test-Path -Path C:\AzCopy\AzCopy.exe
if (!$azcopy_test) {
    cd c:\
    Invoke-WebRequest -Uri "https://aka.ms/downloadazcopy-v10-windows" -OutFile AzCopy.zip -UseBasicParsing
  
    #Expand Archive
    Expand-Archive ./AzCopy.zip ./AzCopy -Force
 
    #Move AzCopy to the destination you want to store it
    Get-ChildItem ./AzCopy/*/azcopy.exe | Move-Item -Destination "C:\AzCopy\AzCopy.exe"
}

cd C:\AzCopy
# need to login azcopy once prior of running script .\AzCopy.exe login --tenant-id $subscription.TenantId

######################## CREATE BLOB ###########################

$location = "westus"
$resourceGroup = "varonis-storage"
$san = "hagaystorageaccount"
$grg = Get-AzResourceGroup

if ($grg.ResourceGroupName -notcontains $resourceGroup) {
    New-AzResourceGroup -Name $resourceGroup -Location $location
}

$gsa = Get-AzStorageAccount

if ($gsa.StorageAccountName -notcontains $san) {

    $storageAccount = New-AzStorageAccount `
        -ResourceGroupName $resourceGroup `
        -AccountName $san `
        -Location $location `
        -SkuName "Standard_LRS" `
        -Kind StorageV2 `

}

$ctx = $storageAccount.Context
$containerName = "varonisblob"
if ($ctx) {
    $gbn = get-AzStorageContainer -Context $ctx
}
else {
    { continue }
}

if ($gbn.name -notcontains $containerName -and $ctx) {
    New-AzStorageContainer -Name $containerName -Context $ctx -Permission blob
}

######################## CREATE SAS AND UPLOAD #####################################

 
Select-AzSubscription -SubscriptionId $subscription.Id
$storageAccountKey = (Get-AzStorageAccountKey -ResourceGroupName $resourceGroup -Name $san).Value[0]
$destinationContext = New-AzStorageContext -StorageAccountName $san -StorageAccountKey $storageAccountKey
$containerSASURI = New-AzStorageContainerSASToken -Context $destinationContext -ExpiryTime(get-date).AddSeconds(3600) -FullUri -Name $containerName -Permission rwdl
.\AzCopy.exe copy $logpath $containerSASURI --recursive

######################### GET LINKS TO FILES #################################

function Get-BlobItems {  
    param (
        $URL
    )

    $uri = $URL.split('?')[0]
    $sas = $URL.split('?')[1]

    $newurl = $uri + "?restype=container&comp=list&" + $sas 

    #Invoke REST API
    $body = Invoke-RestMethod -uri $newurl

    #cleanup answer and convert body to XML
    $xml = [xml]$body.Substring($body.IndexOf('<'))

    #use only the relative Path from the returned objects
    $files = $xml.ChildNodes.Blobs.Blob.Name

    #regenerate the download URL incliding the SAS token
    $files | ForEach-Object { $uri + "/" + $_ + "?" + $sas }    
}

Get-BlobItems $containerSASURI
