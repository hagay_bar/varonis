$Password = Convertto-SecureString -AsPlainText "!QAZ2wsx" -Force
Set-ExecutionPolicy Unrestricted -Force
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools

$Params1 = @{
    CreateDnsDelegation = $false
    DatabasePath = 'C:\Windows\NTDS'
    DomainMode = 'WinThreshold'
    DomainName = 'varonis.com'
    DomainNetbiosName = 'VARONIS'
    ForestMode = 'WinThreshold'
    InstallDns = $true
    LogPath = 'C:\Windows\NTDS'
    NoRebootOnCompletion = $true
    SafeModeAdministratorPassword = $Password
    SysvolPath = 'C:\Windows\SYSVOL'
    Force = $true
}
Install-ADDSForest @Params1
restart-computer -Force
