set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter).InterfaceIndex -ServerAddresses ("10.0.0.5,10.0.0.4")

$username = "varonis\varonis"
$password = "!QAZ2wsx"
$domainname = "varonis.com"
$pass = ConvertTo-SecureString $password -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$pass

write-host " ##################################### TRYING TO ADD SERVER TO DOMAIN #####################################"
Add-Computer -DomainName $domainname -Credential $cred -PassThru -ErrorAction Stop -Restart -Force
