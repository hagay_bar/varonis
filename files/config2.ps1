function func_sleep {
    $x = Get-ADDomain
    if ($x -eq $null) {
        Start-Sleep 1 
        Write-Output "waiting for ADD service to get up"
        func_sleep
    }else {
        Write-Output "domain is up"
        $Params2 = @{
            Name = 'VaronisAssignment1Group'
            SamAccountName = 'VaronisAssignment1Group'
            GroupCategory = 'Security'
            GroupScope = 'Global'
            DisplayName = 'VaronisAssignment1Group'
            Path = 'CN=Users,DC=varonis,DC=Com'
            Description = 'VaronisAssignment1Group members'
        
        }        
        New-ADGroup @Params2        
        New-ADUser -Name "varonis" -AccountPassword (ConvertTo-SecureString -AsPlainText "!QAZ2wsx" -Force) -passThru | Enable-ADAccount        
        Add-ADGroupMember -Identity 'VaronisAssignment1Group' -Members 'varonis' 
    }
}
func_sleep
